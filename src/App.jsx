import logo from './logo.svg';
import './App.css';
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import { EmployeeProvider } from './context/employeeContext'
import { SlocProvider } from './context/slocContext'
import { AuthProvider } from './context/authContext'
import Routing from './routes';


// optional configuration
const options = {
  // you can also just use 'bottom center'
  position: positions.BOTTOM_CENTER,
  timeout: 5000,
  offset: '30px',
  // you can also just use 'scale'
  transition: transitions.SCALE
}

function App() {
  return (
    <AlertProvider template={AlertTemplate} {...options}>
      <AuthProvider>
        <SlocProvider>
          <EmployeeProvider>
            <Routing />
          </EmployeeProvider>
        </SlocProvider>
      </AuthProvider>
    </AlertProvider >
  );
}

export default App;
