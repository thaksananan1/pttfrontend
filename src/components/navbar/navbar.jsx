import React, { useState, useContext, useEffect } from 'react';
import { Link } from "react-router-dom";
import { authContext } from '../../context/authContext'
const NavBar = () => {

    const { authData, LogIn, LogOut } = useContext(authContext);

    useEffect(() => {
        console.log(authData.loggedIn)
    }, [])
    let buttonInNavBar = [
        {
            title: 'Home',
            link: '/'
        },
        {
            title: 'Employee',
            link: '/employee'
        },
    ]
    return (
        <div className="relative flex flex-wrap items-center justify-between px-2 py-3 bg-yellow-500 rounded-b-3xl">
            <div className="flex flex-row justify-between">
                {
                    buttonInNavBar.map((item) => {
                        return (
                            <div className="flex ml-3 text-lg font-bold border-x-violet-50 p-2">
                                <Link to={item.link}>{item.title}</Link>
                            </div>
                        )
                    })
                }
                {authData.loggedIn == true&&<div className="flex ml-3 text-lg font-bold border-x-violet-50 p-2">
                    <Link to="/admin">admin</Link>
                </div>}
            </div>
            <div className="flex ml-6 text-lg font-bold border-x-violet-50 p-2 ">
                {authData.loggedIn == false ? <Link to="/login"><div className="text-lg font-bold">login</div></Link> : <button onClick={() => LogOut()}>Logout</button>}
            </div>
        </div>
    );
}
export default NavBar;