import React from 'react';

const Card = (props) => {
    return (
        <div className="m-5">
            <div className=" bg-white h-72 w-5/12">
                <props/>
            </div>
        </div>
    )
}

export default Card;