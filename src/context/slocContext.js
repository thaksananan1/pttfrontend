import React, { createContext, useReducer } from "react"
import axios from 'axios';

export const slocContext = createContext({})

const slocContextState = {
    sloc: 10,
    max: 10
}

const slocReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_EMPLOYEE_TO_SLOC':
            return {
                ...state, sloc: state.sloc - action.payload
            }
        case 'SUB_EMPLOYEE_TO_SLOC':
            return {
                ...state, sloc: state.sloc + action.payload
            }
        case 'SET_SLOC':
            return {
                ...state, sloc: state.sloc + (action.payload - state.max), max: action.payload
            }
    }
}

export const SlocProvider = ({ children }) => {
    const [slocState, employeeDispatch] = useReducer(slocReducer, slocContextState);

    const sloc = slocState
    const AddEmployeeToSloc = payload => employeeDispatch({ type: 'ADD_EMPLOYEE_TO_SLOC', payload });
    const SubEmployeeToSloc = payload => employeeDispatch({ type: 'SUB_EMPLOYEE_TO_SLOC', payload });
    const SetEmployeeSloc = payload => employeeDispatch({ type: 'SET_SLOC', payload });

    return (
        <slocContext.Provider value={{ sloc, AddEmployeeToSloc, SubEmployeeToSloc, SetEmployeeSloc }}>
            {children}
        </slocContext.Provider>
    )
}
