import React, { createContext, useReducer } from "react"

export const authContext = createContext({});

const initauthState = {
    username: 'admin',
    password: '1234',
    loggedIn: false,
}

const authReducer = (state, payload) => {
    switch (payload.type) {
        case 'LOGIN':
            if (
                payload.payload.username == state.username &&
                payload.payload.password == state.password
            ) {
                console.log('Login successful')
                return { ...state, loggedIn: true };
            }
        case 'LOGOUT': return { ...state, loggedIn: false };
    }
}

export const AuthProvider = ({ children }) => {
    const [authState, authDispatch] = useReducer(authReducer, initauthState)

    const authData = authState
    const LogIn = payload => authDispatch({ type: 'LOGIN', payload });
    const LogOut = () => authDispatch({ type: 'LOGOUT' });

    return (
        <authContext.Provider value={{ authData, LogIn, LogOut }}>
            {children}
        </authContext.Provider>
    )
}