import React, { createContext, useReducer } from "react"
import axios from 'axios';

export const employeeContext = createContext([]);

const employeeContextState = []

const employeeReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_EMPLOYEE': return [...state, action.payload];
        case 'SET_EMPLOYEE_SEAT':
            // console.log(action.payload)
            for (let i in state) {
                if (state[i].firstname == action.payload.firstname && state[i].lastname == action.payload.lastname) {
                    // console.log('find')
                    state[i].seat = action.payload.seat
                }
            }
            // console.log(state)
            return [...state];
    }
}

export const EmployeeProvider = ({ children }) => {
    const [employeeState, employeeDispatch] = useReducer(employeeReducer, employeeContextState)

    const employee = employeeState
    const addEmployee = payload => employeeDispatch({ type: 'ADD_EMPLOYEE', payload })
    const setEmployeeSeat = payload => employeeDispatch({ type: 'SET_EMPLOYEE_SEAT', payload })
    return (
        <employeeContext.Provider value={{ employee, addEmployee, setEmployeeSeat }}>
            {children}
        </employeeContext.Provider>
    )
}