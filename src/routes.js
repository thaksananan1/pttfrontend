import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// import page
import NavBar from "./components/navbar/navbar";
import Home from "./views/home";
import Admin from "./views/admin"
import Employee from "./views/employee"
import Login from "./views/login"

const Routing = () => {
    return (
        <Router >
            <NavBar />
            <Routes >
                <Route path="/" element={<Home />} />
                <Route path="/admin" element={<Admin />} />
                <Route path="/Employee" element={<Employee />} />
                <Route path="/Login" element={<Login />} />
            </Routes>
        </Router>
    )
}

export default Routing