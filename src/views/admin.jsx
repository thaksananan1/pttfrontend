import React, { useState, useContext, useEffect, forceUpdate, useCallback } from "react";
import { employeeContext } from '../context/employeeContext';
import { slocContext } from '../context/slocContext';
import { useAlert } from 'react-alert'
import dayjs from 'dayjs';
import ReactLoading from "react-loading";

const Admin = () => {
    const { employee, addEmployee, setEmployeeSeat } = useContext(employeeContext);
    const { sloc, AddEmployeeToSloc, SubEmployeeToSloc, SetEmployeeSloc } = useContext(slocContext);
    const [Employee, setEmployee] = useState([]);
    const [Number, setNumber] = useState(0);
    const [sort, setSort] = useState("Name");
    const [Chair, setChair] = useState([]);
    const [selectSeat, setSelectSeat] = useState('');
    const [selectUser, setselectUser] = useState({})
    const [loading, setLoading] = useState(false);
    const alert = useAlert()

    useEffect(() => {
        // console.log(employeeState)
        setEmployee(employee);
        arrageArrayforRowChair(sloc.max);
    }, [])

    // function
    const serachWord = text => {
        let searchArray = employee.filter((word) => { return word.firstname.search(text) != -1 })
        setEmployee(searchArray)
    }
    const setSlocMax = () => {
        if (employee.length < 0) {
            alert.show("cannot set sloc")
        } else {
            for (let i in employee) {
                console.log(parseInt(employee[i].seat))
                if (parseInt(employee[i].seat) > Number) {
                    alert.show("cannot set sloc")
                    return;
                }
            }
            SetEmployeeSloc(parseInt(Number))
            ReChair(parseInt(Number))
            alert.show("Set Sloc successfully")
        }

    }

    const switchSort = () => {
        if (sort == "Name") {
            let sortem = employee.sort(function (a, b) { return parseInt(a.seat) - parseInt(b.seat) })
            setEmployee(employee)
            setSort("Seat")
        } else {
            let sortem = employee.sort((obj1, obj2) => { return obj1.firstname.localeCompare(obj2.firstname); })
            console.log(sortem);
            setEmployee(employee)
            setSort("Name")
        }
    }
    
    const arrageArrayforRowChair = (max) => {
        let returnArr = [];
        let Row = [];
        for (let i = 1; i <= max; i++) {
            Row.push({ Empty: true, Seat: i, user: null })
            if (i % 5 == 0 || i == max) {
                returnArr.push(Row)
                Row = [];
            }
        }
        if (employee.length != 0) {
            for (let i in employee) {
                let seat_employee = employee[i].seat;
                console.log("employee ==> ", employee[i])
                let count = 0;
                if (seat_employee == null) break;
                console.log('seat_employee ==> ', seat_employee)
                for (let j in returnArr) {
                    let row = returnArr[j];
                    for (let k in row) {
                        count++;
                        if (count == seat_employee) {
                            returnArr[j][k].Empty = false;
                            returnArr[j][k].user = employee[i].firstname;
                        }
                    }
                }
            }
        }
        setChair(returnArr)
    }

    const ReChair = useCallback((max) => {
        setLoading(true);
        let returnArr = [];
        let Row = [];
        for (let i = 1; i <= max; i++) {
            Row.push({ Empty: true, Seat: i, user: null })
            if (i % 5 == 0 || i == max) {
                returnArr.push(Row)
                Row = [];
            }
        }
        if (employee.length != 0) {
            for (let i in employee) {
                let seat_employee = employee[i].seat;
                console.log("employee ==> ", employee[i])
                let count = 0;
                if (seat_employee == null) break;
                console.log('seat_employee ==> ', seat_employee)
                for (let j in returnArr) {
                    let row = returnArr[j];
                    for (let k in row) {
                        count++;
                        if (count == seat_employee) {
                            returnArr[j][k].Empty = false;
                            returnArr[j][k].user = employee[i].firstname;
                        }
                    }
                }
                setLoading(false);
            }
            setLoading(true)
        }
        setChair(returnArr)
        setLoading(false);

    }, [Chair])

    const setUsertoSeat = () => {
        let user = {
            firstname: Employee[selectUser - 1].firstname,
            lastname: Employee[selectUser - 1].lastname,
            seat: selectSeat
        }

        let sitChair = {}
        let count = 0;
        let max = 0;
        for (let i in Chair) {
            let row = Chair[i]
            for (let j in row) {
                if (Chair[i][j].user == user.firstname) {
                    Chair[i][j].Empty = true;
                    Chair[i][j].user = null;
                }
                count++;
                if (count == selectSeat) {
                    sitChair = Chair[i][j];
                }
                max++;
            }
        }
        sitChair.Empty = false;
        sitChair.user = user.firstname
        setEmployeeSeat(user)
        SetEmployeeSloc(max)
        ReChair(max)
        arrageArrayforRowChair(sloc.max)
    }




    return (
        <div className="h-screen flex bg-white  text-center ">
            <div className="flex flex-row h-full w-full justify-center">
                <div className=" flex flex-col bg-slate-200 m-6 p-5  rounded-lg h-5/6 w-2/6 items-center">
                    {loading ?
                        <div className="flex h-full w-full items-center justify-center"><ReactLoading type="spin" color="#FFF" /></div>
                        :
                        <div>
                            <div className="flex bg-orange-500 rounded-b-2xl h-28 w-full justify-center items-center">
                                <h1 className="items-center text-4xl">เวที</h1>
                            </div>
                            <div className="flex justify-center flex-col mt-5">
                                {
                                    Chair.map((row) => {
                                        return (<div className="flex flex-row justify-start">
                                            {row.map((item) => {
                                                return (
                                                    <div className={item.Empty ?
                                                        "flex bg-green-700 w-20 h-20 justify-center font-bold items-center m-4 rounded-xl flex-col" :
                                                        "flex bg-red-500 w-20 h-20 justify-center font-bold text-white items-center m-4 rounded-xl flex-col"
                                                    }><div>A{item.Seat}</div><div>{item.Empty ? "ว่าง" : item.user}</div></div>)
                                            })}
                                        </div>
                                        )
                                    })
                                }
                            </div>
                        </div>}
                </div>
                <div className="flex flex-col h-5/6 w-4/6 m-5 justify-between">
                    <div className="bg-slate-200 w-full text-center items-center rounded-lg h-4/6 m-1">
                        <div className="flex flex-row justify-center text-center text-4xl m-3">
                            <h1 className="flex items-center ">List Employee </h1>
                            {/* <div className="flex items-center m-2">{sloc.sloc > 0 ? sloc.sloc : "Empty"}</div> */}
                        </div>
                        <div className="text-xl text-left m-2">
                            <div className="p-2 text-center">
                                <div className="flex justify-around items-center">
                                    <div className="flex text-center ">search : </div>
                                    <input type="text" className="shadow appearance-none border rounded w-3/5 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="search..." onChange={(e) => { serachWord(e.target.value) }} />
                                    <div className="flex w-1/5  justify-around items-center">
                                        <div className="flex justify-center text-center">sort by :</div>
                                        <button className="bg-gray-800 rounded-md  w-36 h-11 text-white" onClick={() => { switchSort() }}>{sort}</button>
                                    </div>
                                </div>
                            </div>
                            <div className=" bg-white rounded-md p-2 m-2">
                                <div className="flex flex-row p-1.5 border-1 bg-slate-400 justify-around border-b-2 rounded font-medium">
                                    <div className="text-center p-1 w-10 ">No</div>
                                    <div className="text-center p-1 w-72 ">Name</div>
                                    <div className="text-center p-1 w-36 ">Phone</div>
                                    <div className="text-center p-1 w-36 ">Seat</div>
                                    <div className="text-center p-1 w-36 ">Date</div>
                                    <div className="text-center p-1 w-36 ">Time</div>
                                </div>
                                <div className="overflow-y-auto  h-72">
                                    {/* List Employee */}
                                    {Employee.map((item, index) => {
                                        return (
                                            // <div className="m-3 p-1.5 border-1 rounded-md bg-amber-400">
                                            //     {index + 1}.name : {item.firstname} {item.lastname} phone : {item.phone} Date : c Time: {dayjs(item.at).format('HH:mm')}
                                            // </div>
                                            <div
                                                className={
                                                    (index + 1) % 2 == 0 ?
                                                        "flex flex-row p-1.5 border-1 bg-slate-100 justify-around drop-shadow-md border-b-2 border-slate-300 m-0.5 rounded" :
                                                        "flex flex-row p-1.5 border-1 bg-slate-200 justify-around drop-shadow-md border-b-2 border-slate-300 m-0.5 rounded"
                                                }>
                                                <div className="text-center p-1 w-10 ">{index + 1}</div>
                                                <div className="text-center p-1 w-72 ">{item.firstname} {item.lastname}</div>
                                                <div className="text-center p-1 w-36 ">{item.phone}</div>
                                                <div className="text-center p-1 w-36 ">{item.seat != null && 'A' + item.seat}</div>
                                                <div className="text-center p-1 w-36 ">{dayjs(item.at).format('DD/MM/YYYY')}</div>
                                                <div className="text-center p-1 w-36 ">{dayjs(item.at).format('HH:mm')}</div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="flex justify-between bg-slate-200  w-full rounded-lg h-2/6 m-1 p-10">
                        <div className="flex flex-col">
                            <h1 className="text-xl font-extrabold text-left">Setup Employee Sloc</h1>
                            <div className="">
                                <input type="text" className="shadow appearance-none border rounded w-72 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="max..." onChange={(e) => { setNumber(e.target.value) }} />
                                <button className="bg-green-600 rounded-md m-3 pl-10 pr-10 pt-1.5 pb-1.5" onClick={() => { setSlocMax() }}>
                                    set
                                </button>
                            </div>
                            <h1 className="text-xl font-extrabold text-left">Inviter Seat</h1>
                            <div className="flex flex-row justify-evenly">
                                <div className="flex flex-row justify-center items-center ">
                                    seat : <input type="text" className="shadow appearance-none border rounded w-20 ml-5 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Seat" onChange={(e) => { setSelectSeat(e.target.value) }} />
                                </div>
                                <div className="flex flex-row justify-center items-center ml-5">
                                    ID : <input type="text" className="shadow appearance-none border rounded w-20 ml-5 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="No" onChange={(e) => { setselectUser(e.target.value) }} />
                                </div>
                                <button className="bg-green-600 rounded-md m-3 pl-8 pr-8 pt-1.5 pb-1.5" onClick={() => { setUsertoSeat() }}>
                                    Enter
                                </button>
                            </div>
                        </div>
                        <div className="flex flex-col">
                            <h1 className="items-center text-4xl">Empty Sloc</h1>
                            <div className="items-center text-9xl m-2">{sloc.sloc}</div>
                        </div>
                        <div className="flex flex-col">
                            <h1 className="items-center text-4xl">Max Sloc</h1>
                            <div className="items-center text-9xl m-2">{sloc.max}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}
export default Admin;