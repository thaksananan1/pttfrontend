import React, { useState, useContext, useEffect } from "react";
import { employeeContext } from '../context/employeeContext';
import { slocContext } from '../context/slocContext';
import { Link } from 'react-router-dom';

const Home = () => {
    const { employee, addEmployee } = useContext(employeeContext);
    const { sloc, AddEmployeeToSloc, SubEmployeeToSloc } = useContext(slocContext);

    return (
        <div className="h-screen justify-center text-center">
            <h1 className="text-5xl font-extrabold">Homepage</h1>
            <div className="m-6">
                <h1 className="text-5xl font-extrabold">Free Employee Sloc : {sloc.sloc}</h1>
                {sloc.sloc > 0 && <Link to="/employee"><button className="m-5 bg-blue-700 p-5 text-xl rounded-lg text-white">Go to Employee Sloc</button></Link>}
            </div>
        </div>
    )
}
export default Home;