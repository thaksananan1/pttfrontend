import React, { useState, useContext, useEffect } from "react";
import { employeeContext } from '../context/employeeContext';
import { slocContext } from '../context/slocContext';
import { useAlert } from 'react-alert'
import dayjs from 'dayjs';
const Employee = () => {

    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [phone, setPhone] = useState('');
    const { employee, addEmployee } = useContext(employeeContext);
    const { sloc, AddEmployeeToSloc, SubEmployeeToSloc } = useContext(slocContext);
    const [Employee, setEmployee] = useState([]);
    const [Chair, setChair] = useState([]);
    const alert = useAlert()

    useEffect(() => {
        // console.log(employeeState)
        setEmployee(employee);
        arrageArrayforRowChair(sloc.max);
    }, [])

    const onSubmit = e => {
        e.preventDefault()
        const payload = {
            firstname,
            lastname,
            phone,
            at: dayjs(),
            seat: null
        }
        // console.log(payload);
        // console.log(sloc);



        if (firstname != '' && lastname != '' && phone != '') {
            if (sloc.sloc > 0) {
                AddEmployeeToSloc(1);
                Employee.push(payload);
                setFirstname('');
                setLastname('');
                setPhone('');
            } else {
                alert.show("Sloc is Full!");
            }
        }
    }
    const serachWord = text => {
        let searchArray = employee.filter((word) => { return word.firstname.search(text) != -1 })
        setEmployee(searchArray)
    }
    const arrageArrayforRowChair = (max) => {
        let returnArr = [];
        let Row = [];
        for (let i = 1; i <= max; i++) {
            Row.push({ Empty: true, Seat: i, user: null })
            if (i % 5 == 0 || i == max) {
                returnArr.push(Row)
                Row = [];
            }
        }
        if (employee.length != 0) {
            for (let i in employee) {
                let seat_employee = employee[i].seat;
                console.log("employee ==> ", employee[i])
                let count = 0;
                if (seat_employee == null) break;
                console.log('seat_employee ==> ', seat_employee)
                for (let j in returnArr) {
                    let row = returnArr[j];
                    for (let k in row) {
                        count++;
                        if (count == seat_employee) {
                            returnArr[j][k].Empty = false;
                            returnArr[j][k].user = employee[i].firstname;
                        }
                    }
                }
            }
        }
        setChair(returnArr)
    }

    return (
        <div className="h-screen bg-white justify-center text-center">
            {/* <h1 className="text-5xl font-extrabold">Employee</h1> */}
            <div className="flex flex-row h-5/6 justify-center text-center">
                <div className="flex flex-row justify-center items-center bg-slate-300 m-5 p-5 w-1/3 rounded-md drop-shadow-xl ">
                    <form onSubmit={onSubmit} class="flex flex-col text-left">
                        <div className="text-center text-lg font-extrabold">
                            Enter Your Name
                        </div>
                        {/* Firstname Input */}
                        <label className="block text-gray-700 text-md font-bold mb-5">firstname:
                            <input
                                type="text"
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                placeholder="Enter firstname"
                                onChange={e => setFirstname(e.target.value)}
                            />
                        </label>
                        {/* Lastname Input */}
                        <label className="block text-gray-700 text-md font-bold mb-5">lastname:
                            <input
                                type="text"
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                placeholder="Enter lastname"
                                onChange={e => setLastname(e.target.value)} />
                        </label>
                        {/* Phone Input */}
                        <label className="block text-gray-700 text-md font-bold mb-5">phone:
                            <input
                                type="text"
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                placeholder="Enter phone"
                                onChange={e => setPhone(e.target.value)} />
                        </label>
                        <button type="submit" className="bg-green-600 rounded-md h-10 m-10">
                            Submit
                        </button>
                    </form>
                </div>
                <div className="flex flex-col items-center bg-slate-300 drop-shadow-xl  m-5 p-5 w-1/3 rounded-md">
                    <div className="flex bg-orange-500 rounded-b-2xl h-28 w-full justify-center items-center">
                        <h1 className="items-center text-4xl">เวที</h1>
                    </div>
                    <div className="flex justify-center flex-col mt-5">
                        {
                            Chair.map((row) => {
                                return (<div className="flex flex-row justify-start">
                                    {row.map((item) => {
                                        return (
                                            <div className={item.Empty ?
                                                "flex bg-green-700 w-20 h-20 justify-center font-bold items-center m-4 rounded-xl flex-col" :
                                                "flex bg-red-500 w-20 h-20 justify-center font-bold text-white items-center m-4 rounded-xl flex-col"
                                            }><div>A{item.Seat}</div><div>{item.Empty ? "ว่าง" : item.user}</div></div>)
                                    })}
                                </div>
                                )
                            })
                        }
                    </div>
                    <div className="flex flex-col items-end justify-end w-full h-full">
                        Waiting for admin...
                    </div>
                </div>
                
            </div>
        </div>
    )
}
export default Employee;