import React, { useState, useContext, useEffect } from "react";
import { authContext } from '../context/authContext'

const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const { authData, LogIn, LogOut } = useContext(authContext);

    const onSubmit = e => {
        e.preventDefault()
        let payload = {
            username : 'admin',
            password : '1234'
        }
        LogIn(payload)
    }

    return (
        <div>
            <h1>Login page</h1>
            {/* <h1>LoggedIn : {(authData.loggedIn).toString()}</h1> */}
            <div className="bg-slate-400 m-5 p-5 w-3/3 rounded-md">
                <form onSubmit={onSubmit} className="flex flex-col text-left">
                    <div className="text-center text-lg font-extrabold">
                        Enter Your Name
                    </div>
                    {/* Firstname Input */}
                    <label className="block text-gray-700 text-md font-bold mb-5">username :
                        <input
                            type="text"
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            placeholder="Enter Username"
                            onChange={e => setUsername(e.target.value)}
                        />
                    </label>
                    {/* Lastname Input */}
                    <label className="block text-gray-700 text-md font-bold mb-5">password :
                        <input
                            type="text"
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            placeholder="Enter Password"
                            onChange={e => setPassword(e.target.value)} />
                    </label>
                    <button type="submit" className="bg-green-600 rounded-md h-10 m-10">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    )
}
export default Login;